﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab06Lib;

namespace lab06
{
    class Program
    {
        static void Main(string[] args)
        {
            Telephone_network_subscribers[] arrTelep;

            Console.Write(" num of listed subscribers: ");
            int cntTelephone_network_subscribers = int.Parse(Console.ReadLine());
            arrTelep = new Telephone_network_subscribers[cntTelephone_network_subscribers];

            for (int i = 0; i < cntTelephone_network_subscribers; i++)
            {
                Console.Write(" the name of user: ");
                string sName = Console.ReadLine();

                Console.Write(" country/region: ");
                string sCountry = Console.ReadLine();

                Console.Write(" rate: ");
                string sRate = Console.ReadLine();

                Console.Write("  email : ");
                string sEmail = Console.ReadLine();

                Console.Write("money_in_the_account : ");
                string sMoney_in_the_account = Console.ReadLine();

                Console.Write("average life expectancy: ");
                string sYears = Console.ReadLine();

                Console.Write(" does it have smartphone ? (y-yes, n-no): ");
                ConsoleKeyInfo keyHas_a_smartphone = Console.ReadKey();
                Console.WriteLine();

                Console.Write(" is it a Solvent? (y-yes, n-no): ");
                ConsoleKeyInfo keySolvent = Console.ReadKey();
                Console.WriteLine();

                Telephone_network_subscribers OurTelephone_network_subscriber = new Telephone_network_subscribers();

                OurTelephone_network_subscriber.Name = sName;
                OurTelephone_network_subscriber.Country = sCountry;
                OurTelephone_network_subscriber.Rate = sRate;
                OurTelephone_network_subscriber.Email = int.Parse(sEmail);
                OurTelephone_network_subscriber.Money_in_the_account = double.Parse(sMoney_in_the_account);
                OurTelephone_network_subscriber.Years = double.Parse(sYears);
                OurTelephone_network_subscriber.Has_a_smartphone = keyHas_a_smartphone.Key == ConsoleKey.Y ? true : false;
                OurTelephone_network_subscriber.Solvent = keySolvent.Key == ConsoleKey.Y ? true : false;

                arrTelep[i] = OurTelephone_network_subscriber;

            }
            foreach (Telephone_network_subscribers s in arrTelep)
            {
                Console.WriteLine();
                Console.WriteLine("------------------------------------------------");
                Console.WriteLine(" current data about {0}," + s.Name);
                Console.WriteLine("------------------------------------------------");
                Console.WriteLine(" name: " + s.Name);
                Console.WriteLine(" country: " + s.Country);
                Console.WriteLine(" rate: " + s.Rate);
                Console.WriteLine("email: " + s.Email.ToString());
                Console.WriteLine(" money_in_the_account : " + s.Money_in_the_account.ToString("0"));
                Console.WriteLine(" average of living: " + s.yearsofTelephone_network_subscribers.ToString("0.00"));
                Console.WriteLine(s.Has_a_smartphone ? " Has a smartphone" : " no a smartphone");
                Console.WriteLine(s.Solvent ? " solvent" : " insolvent");
                Console.WriteLine();

            }
                Console.ReadKey();
            }
        }
    }
