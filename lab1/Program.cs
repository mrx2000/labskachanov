﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Варiант 11\nВведiть початкове значення Xmin: ");
            string sx = Console.ReadLine();
            double xMin = Double.Parse(sx, System.Globalization.CultureInfo.InvariantCulture);
            Console.Write("Введiть кiнцеве значення Xmax: ");
            sx = Console.ReadLine();
            double xMax = double.Parse(sx, System.Globalization.CultureInfo.InvariantCulture);
            Console.Write("Введiть прирiст dX: ");
            sx = Console.ReadLine();
            double dx = double.Parse(sx, System.Globalization.CultureInfo.InvariantCulture);
            double x1 = xMin;
            double y;
            double part1;
            double part2;
            double res = 1;

            Console.WriteLine();
            for (; x1 <= xMax; x1 += dx)
            {
                double x2 = 3 * x1;
                part1 = xMin + x2 + Math.Sin(xMin * x2);
                part2 = part1 / 5 * Math.Cos(x2 * x2);
                y = Math.Sqrt(56 + part2);
                double midres = y;
                res *= midres;
                Console.WriteLine("x = {0:0.000}\t\t y = {1:0.000}\t\t\t//y full = {2:0.00000}", x1, y, midres);
            }
            Console.WriteLine("\n\nResult = {0:0.000000}.", res);
            Console.ReadKey();
        }
    }
}
