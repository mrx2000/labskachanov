﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab08
{
    public partial class fTelephone_network_subscriber : Form
    {
        public _Telephone_network_subscriber OurTelephone_network_subscriber;
        public fTelephone_network_subscriber(_Telephone_network_subscriber t)
        {
            OurTelephone_network_subscriber = t;   
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            OurTelephone_network_subscriber.Name = tbName.Text.Trim();
            OurTelephone_network_subscriber.Country = tbCountry.Text.Trim();
            OurTelephone_network_subscriber.Rate = tbRate.Text.Trim();
            OurTelephone_network_subscriber.Email = int.Parse(tbEmail.Text.Trim());
            OurTelephone_network_subscriber.Money_in_the_account =double.Parse(tbMoney_in_the_account.Text.Trim());
            OurTelephone_network_subscriber.Years = double.Parse(tbYears.Text.Trim());
            Has_a_smartphone.Checked = checkHas_a_smartphon.Checked;
            HasSolvent.Checked = OurTelephone_network_subscriber.SolventcheckPredator.Checked;
            DialogResult = DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

        }

        private void fTelephone_network_subscriber_Load(object sender, EventArgs e)
        {
            if (OurTelephone_network_subscriber != null)
            {
                tbName.Text = OurTelephone_network_subscriber.Name;
                tbCountry.Text = OurTelephone_network_subscriber.Country;
                tbRate.Text = OurTelephone_network_subscriber.Rate;
                tbEmail.Text = OurTelephone_network_subscriber.Email.ToString();
                tbMoney_in_the_account.Text = OurTelephone_network_subscriber.Money_in_the_account.ToString("0.00");
                tbYears.Text = OurTelephone_network_subscriber.Years.ToString("0.000");
                chbHas_a_smartphone.Checked = OurTelephone_network_subscriber.Has_a_smartphone;
                chbSolvent.Checked = OurTelephone_network_subscriber.Solvent;
            }
        }
    }
}
public class Telephone_network_subscribers

{
    public string Name;
    public string Country;
    public string Rate;
    public int Email;
    public double Money_in_the_account;
    public double Years;
    public bool Has_a_smartphone;
    public bool Solvent;


    public double yearsofTelephone_network_subscribers { get { return YearsofLiving(); } }
    public double YearsofLiving()
    {
        return Years;
    }

}

