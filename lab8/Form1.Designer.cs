﻿namespace lab08
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Telephone_network_subscriber = new System.Windows.Forms.TextBox();
            this.btnAddTelephone_network_subscriber = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Telephone_network_subscriber
            // 
            this.Telephone_network_subscriber.Location = new System.Drawing.Point(12, 23);
            this.Telephone_network_subscriber.Multiline = true;
            this.Telephone_network_subscriber.Name = "Telephone_network_subscriber";
            this.Telephone_network_subscriber.ReadOnly = true;
            this.Telephone_network_subscriber.Size = new System.Drawing.Size(611, 305);
            this.Telephone_network_subscriber.TabIndex = 0;
            // 
            // btnAddTelephone_network_subscriber
            // 
            this.btnAddTelephone_network_subscriber.Location = new System.Drawing.Point(644, 23);
            this.btnAddTelephone_network_subscriber.Name = "btnAddTelephone_network_subscriber";
            this.btnAddTelephone_network_subscriber.Size = new System.Drawing.Size(123, 23);
            this.btnAddTelephone_network_subscriber.TabIndex = 1;
            this.btnAddTelephone_network_subscriber.Text = "Додати абонента";
            this.btnAddTelephone_network_subscriber.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(702, 280);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Закрити";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnAddTelephone_network_subscriber);
            this.Controls.Add(this.Telephone_network_subscriber);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "\"Лабораторна робота №8\" ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Telephone_network_subscriber;
        private System.Windows.Forms.Button btnAddTelephone_network_subscriber;
        private System.Windows.Forms.Button btnClose;
    }
}

