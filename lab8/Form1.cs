﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab08
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnAddTelephone_network_subscriber_Click(object sender, EventArgs e)
        {
            Telephone_network_subscriber telephone_network_subscriber = new _Telephone_network_subscriber();
           Form1 ft = new Form1(t);
            if (ft.ShowDialog() == DialogResult.OK)
            {
                tInfo.Text += string.Format("{0}, {1}, {2}. Email: {3}. Money: {5} [{6} | {7}] | Years : {8:0.00} years\r\n", t.Name, t.Country, t.Rate, t.Email, t.Money_in_the_account, t.Years, t.Has_a_smartphone ? " has smartphone" : " no smartphone", t.Solvent ? " solvent" : " insolvent", t.YearsofLiving());

            }
        }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Припинити роботу застосунку?",
            "Припинити роботу", MessageBoxButtons.OKCancel,
             MessageBoxIcon.Question) == DialogResult.OK)
                Application.Exit();
        }
    }

