﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab02
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Варiант 11\n Введiть x1");
            double x1 = Double.Parse(Console.ReadLine());
            Console.WriteLine(" Введiть x1 Max");
            double x1_max = Double.Parse(Console.ReadLine());
            Console.WriteLine(" Введiть delta x1");
            double d_x1 = Double.Parse(Console.ReadLine());
            Console.WriteLine(" Введiть  x2");
            double x2 = Double.Parse(Console.ReadLine());
            Console.WriteLine("  Введiть   x2 Max");
            double x2_max = Double.Parse(Console.ReadLine());
            Console.WriteLine(" Введiть  delta x2");
            double d_x2 = Double.Parse(Console.ReadLine());
            double multiplication =1 ;
            Console.Write("X2\\X1\t");
            for (double x1_min = x1; x1_min <= x1_max; x1_min += d_x1)
            {
                Console.Write("{0:f4}\t", x1_min);
            }
            Console.WriteLine();
            for (double x2_min = x2; x2_min <= x2_max; x2_min += d_x2)
            {
                Console.Write("{0:f4}\t", x2_min);
                for (double x1_min = x1; x1_min <= x1_max; x1_min += d_x1)
                {
                    double y1 = 5* Math.Sqrt(Math.Pow(x1, 3)+ Math.Pow(x2, 5)- Math.Cos(x2));
                    double y = y1 / Math.Exp(x1);
                    Console.Write("{0:f4}\t", y);
                    if (y<0)
                    {

                        multiplication *= y;
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine(" multiplication: {0:f4}", multiplication);
            Console.ReadKey();

        }
    }
}
